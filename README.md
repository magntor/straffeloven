# Straffeloven 2005 - se [lovdata](https://lovdata.no/dokument/NL/lov/2005-05-20-28/KAPITTEL_2-6#KAPITTEL_2-6)
* Loven kan anvendes f.eks ved:
    * § 204. Innbrudd i datasystem
        * Med bot eller fengsel inntil 2 år straffes den som ved å bryte en beskyttelse eller ved annen uberettiget fremgangsmåte skaffer seg tilgang til datasystem eller del av det.
    * § 205. Krenkelse av retten til privat kommunikasjon
        * Med bot eller fengsel inntil 2 år straffes den som uberettiget <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    a)	og ved bruk av teknisk hjelpemiddel hemmelig avlytter eller gjør hemmelig opptak av telefonsamtale<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   eller annen kommunikasjon mellom andre, eller av forhandlinger i lukket møte som han ikke selv deltar i, eller som han uberettiget har skaffet seg tilgang til, <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    b)	bryter en beskyttelse eller på annen uberettiget måte skaffer seg tilgang til informasjon som overføres ved elektroniske eller andre tekniske hjelpemidler, <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    c)	åpner brev eller annen lukket skriftlig meddelelse som er adressert til en annen, eller på annen måte skaffer seg uberettiget tilgang til innholdet, eller <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    d)	hindrer eller forsinker adressatens mottak av en meddelelse ved å skjule, endre, forvanske, ødelegge eller holde meddelelsen tilbake. <br>